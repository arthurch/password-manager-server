#include <stdio.h>
#include <string.h>

#include <websocket/websocket.h>
#include <utils/utils.h>

void onconnection(ws_server_t *srv, ws_client_t *client)
{
  printf("Yea ! we got a connection ! Welcome !\n");
}

int main(int argc, char **argv)
{
  printf("\tWebSocket server test\n");

  ws_server_t *srv = ws_server_setup(8000, 2);
  if(srv == NULL)
  {
    return -1;
  }
  ws_wait(srv, onconnection);

  char buffer[16384];
  uint8_t opcode;
  size_t len = ws_recv(&(srv->client), buffer, 16384, &opcode);
  printf("Received %lu bytes (opcode=0x%x)\n", len, opcode);
  hexdump("data hexdump", buffer, len);

  strcpy(buffer, "hey client i'm the server !!!");
  ws_send(&(srv->client), buffer, strlen(buffer), WS_SEND_TEXT);

  ws_close_server(srv);

  return 0;
}
