#include <utils/utils.h>

#include <stdio.h>
#include <string.h>

#include <stdint.h>

uint64_t ntoh64(const uint64_t input)
{
  uint64_t rval;
  uint8_t *data = (uint8_t *)&rval;

  data[0] = input >> 56;
  data[1] = input >> 48;
  data[2] = input >> 40;
  data[3] = input >> 32;
  data[4] = input >> 24;
  data[5] = input >> 16;
  data[6] = input >> 8;
  data[7] = input >> 0;

  return rval;
}

uint64_t hton64(const uint64_t input)
{
  return ntoh64(input);
}

int startswith(char *str, char *start)
{
  unsigned int i;
  for(i = 0; i<strlen(start); i++)
  {
    if(str[i] != start[i])
    {
      return 0;
    }
  }
  return 1;
}

char *strsplitlast(char *str, char split)
{
  unsigned i;
  int found = 0;
  for(i = 0; i<strlen(str); i++)
  {
    if(found)
    {
      if(str[i] != ' ')
      {
        return str+i;
      }
    }
    if(str[i] == split)
    {
      found = 1;
    }
  }
  return NULL;
}

void hexdump(const char * desc, const void * addr, const size_t len)
{
  int i;
  unsigned char buff[17];
  const unsigned char *pc = (const unsigned char *) addr;

  // Output description if given.
  if(desc != NULL)
      printf("%s\n", desc);

  // Length checks.
  if(len == 0)
  {
    printf("  no data...\n");
    return;
  }

  // Process every byte in the data.
  for(i = 0; i < len; i++)
  {
    // Multiple of 16 means new line (with line offset).
    if((i % 16) == 0)
    {
      // Don't print ASCII buffer for the "zeroth" line.
      if(i != 0)
          printf("  |%s|\n", buff);

      // Output the offset.
      printf("  %04x ", i);
    }

    // Now the hex code for the specific character.
    printf(" %02x", pc[i]);

    // And buffer a printable ASCII character for later.
    if((pc[i] < 0x20) || (pc[i] > 0x7e)) // isprint() may be better.
        buff[i % 16] = '.';
    else
        buff[i % 16] = pc[i];
    buff[(i % 16) + 1] = '\0';
  }

  // Pad out last line if not exactly 16 characters.
  while((i % 16) != 0)
  {
    printf("   ");
    i++;
  }

  // And print the final ASCII buffer.
  if(strlen((char *) buff) < 16)
  {
    for(i = strlen((char *) buff); i<16; i++)
    {
      buff[i] = ' ';
    }
  }
  printf("  |%s|\n", buff);
}
