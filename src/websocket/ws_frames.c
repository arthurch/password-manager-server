#include <websocket/websocket.h>

#include <errno.h>
#include <string.h>
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>

#include <utils/utils.h>

int ws_send(ws_client_t *client, void *buffer, size_t len, int flag)
{
  size_t frame_length = len;
  size_t header_sz = 0;
  if(len < 126)
  {
    header_sz = sizeof(struct ws_frame_header);
  } else if(len < __INT16_MAX__)
  {
    header_sz = sizeof(struct ws_frame_header) + sizeof(uint16_t);
  } else
  {
    header_sz = sizeof(struct ws_frame_header) + sizeof(uint64_t);
  }
  frame_length += header_sz;

  uintptr_t frame = (uintptr_t) malloc(frame_length);
  struct ws_frame_header *frame_header = (struct ws_frame_header *) frame;
  frame_header->fin = 1;
  frame_header->rsv1 = 0;
  frame_header->rsv2 = 0;
  frame_header->rsv3 = 0;

  frame_header->opcode = ((flag & WS_SEND_TYPE_MASK) == WS_SEND_TEXT) ? WS_OPCODE_TEXT : WS_OPCODE_BINARY;
  if(len < 126)
  {
    frame_header->mask_and_len1 = len & WS_LEN1_MASK;
  } else if(len < __INT16_MAX__)
  {
    frame_header->mask_and_len1 = 126 & WS_LEN1_MASK;
    *((uint16_t *) (frame + sizeof(struct ws_frame_header))) = htons(len);
  } else
  {
    frame_header->mask_and_len1 = 127 & WS_LEN1_MASK;
    *((uint64_t *) (frame + sizeof(struct ws_frame_header))) = hton64(len);
  }

  /* copying actual data */
  memcpy((void *) (frame + header_sz), buffer, len);


#ifdef WS_VERBOSE
  printf("websocket: Sending %lu bytes to client (actual data length : %lu bytes) (flag is 0x%x)...\n", frame_length, len, flag);
#endif

#ifdef WS_DATA_DUMP
  hexdump("websocket: frame hexdump", (void *) frame, (frame_length > WS_DATA_DUMP_SIZE) ? WS_DATA_DUMP_SIZE : frame_length);
  if(frame_length > WS_DATA_DUMP_SIZE)
  {
    printf("  ...\n");
  }
#endif

  size_t ret = send(client->sock.sockfd, (void *) frame, frame_length, 0);
  if(ret == -1)
  {
#ifndef WS_NO_ERROR
    printf("websocket: [ERROR] Failed to send frame to client\n");
#endif
    free((void *) frame);
    return -1;
  } else if(ret != frame_length)
  {
#ifndef WS_NO_WARNING
    printf("websocket: [WARNING] We wanted to send %lu bytes to client, but only %lu was actually sent\n", frame_length, ret);
#endif
  }

  free((void *) frame);

  return ret;
}

int ws_recv(ws_client_t *client, void *buffer, size_t len, uint8_t *opcode)
{
  char buf[WS_BUFFER_SIZE];

  unsigned int i;

  struct ws_frame_header *frame_header;
  uint8_t *payload = NULL;
  uint8_t *big_payload_ptr = NULL;

  size_t header_sz;
  size_t bytes_recv;

  uint8_t masking_key[5];
  memset(masking_key, 0, 5);

  size_t total_length = 0;
  size_t seek = 0;

  uint8_t started = 0;
  uint8_t fin = 0;
  while(fin == 0)
  {
    bytes_recv = recv(client->sock.sockfd, buf, WS_BUFFER_SIZE, 0);
    if(bytes_recv == -1)
    {
#ifndef WS_NO_ERROR
      printf("websocket: [ERROR] Failed to receive data from client, errnor=%d - \"%s\"\n", errno, strerror(errno));
#endif
      return -1;
    }

    frame_header = (struct ws_frame_header *) buf;
#ifdef WS_DATA_DUMP
    hexdump("websocket: frame header", frame_header, 32);
#endif

#ifdef WS_VERBOSE_DETAILS
    printf("websocket: New WebSocket frame : fin=%d, rsv1=%d, rsv2=%d, rsv3=%d, mask=%d\n",
      frame_header->fin, frame_header->rsv1, frame_header->rsv2, frame_header->rsv3, (frame_header->mask_and_len1 & WS_MASK_MASK) ? 1 : 0);
#endif

    if(started && frame_header->opcode != WS_OPCODE_CONTINUATION)
    {
      /* error: message already started */
#ifndef WS_NO_ERROR
      printf("websocket: [ERROR] didn't received WS_OPCODE_CONTINUATION while message is already started\n");
#endif
      return -1;
    } else if(!started && frame_header->opcode == WS_OPCODE_CONTINUATION)
    {
      /* error: can't continuate if the message hasn't started yet */
#ifndef WS_NO_ERROR
      printf("websocket: [ERROR] received WS_OPCODE_CONTINUATION but message is not started yet\n");
#endif
      return -1;
    }

    if(!started)
    {
      /* it was the first frame */
      if(opcode != NULL)
        *opcode = frame_header->opcode;
      started = 1;
    }

    /* get payload length */
    size_t length = 0;
    uint8_t len1 = frame_header->mask_and_len1 & WS_LEN1_MASK;
    if(len1 < 126)
    {
      length = len1;
      header_sz = sizeof(struct ws_frame_header);
    } else if(len1 == 126)
    {
      length = ntohs(*((uint16_t *) (((uintptr_t) frame_header) + sizeof(struct ws_frame_header))));
      header_sz = sizeof(struct ws_frame_header) + sizeof(uint16_t);
    } else if(len1 == 127)
    {
      length = ntoh64(*((uint64_t *) (((uintptr_t) frame_header) + sizeof(struct ws_frame_header))));
      header_sz = sizeof(struct ws_frame_header) + sizeof(uint64_t);
    }
#ifdef WS_VERBOSE_DETAILS
    printf("websocket: Payload length is %lu bytes\n", length);
#endif

    payload = (uint8_t *) (((uintptr_t) frame_header) + header_sz + WS_MASKINGKEY_SIZE);

    if(total_length+length > len)
    {
#ifndef WS_NO_WARNING
      printf("websocket: [WARNING] received data excess buffer limit, we'll just ignore the remaining data\n");
#endif
      length = len - total_length;

#ifdef WS_VERBOSE_DETAILS
      printf("websocket: remaining length is %lu bytes\n", length);
#endif
    }

    total_length += length;

    if(length > WS_BUFFER_SIZE)
    {
#ifndef WS_NO_WARNING
      printf("websocket: [WARNING] received data excess WS_BUFFER_SIZE, we'll try to allocate a new buffer\n");
#endif
      /* our buffer is too small, getting what's left over */
      big_payload_ptr = (uint8_t *) malloc(length);
      memcpy(big_payload_ptr, payload, bytes_recv);

      size_t ret = recv(client->sock.sockfd, (void *) (((uintptr_t) big_payload_ptr) + bytes_recv), length - bytes_recv, MSG_WAITALL);
      if(ret == -1)
      {
#ifndef WS_NO_ERROR
        printf("websocket: [ERROR] Failed to receive left over data from client, errnor=%d - \"%s\"\n", errno, strerror(errno));
#endif
        return -1;
      }
      bytes_recv += ret;
      payload = big_payload_ptr;
    }

    /* according specification, client is expected to send masked data */
    if(!(frame_header->mask_and_len1 & WS_MASK_MASK))
    {
      /* client is supposed to send masked data */
#ifndef WS_NO_WARNING
      printf("websocket: [WARNING] Client should send masked data\n");
#endif
      for(i = 0; i<length; i++)
      {
        ((uint8_t *) buffer)[seek + i] = payload[i];
      }
    } else
    {
      /* unmask payload */
      memcpy(masking_key, (uint8_t *) (((uintptr_t) frame_header) + header_sz), WS_MASKINGKEY_SIZE);

      for(i = 0; i<length; i++)
      {
        /* btw let's just copy it to our final buffer */
        ((uint8_t *) buffer)[seek + i] = payload[i] ^ masking_key[i % WS_MASKINGKEY_SIZE];
      }
    }

    if(frame_header->fin)
    {
      /* last packet */
      fin = 1;
    } else
    {
      /* continuation frame incoming */
      seek = total_length;
    }

    if(big_payload_ptr != NULL)
    {
      free(big_payload_ptr);
    }
  }

#ifdef WS_VERBOSE
  printf("websocket: We successfully received %lu bytes\n", total_length);
#endif

#ifdef WS_DATA_DUMP
  hexdump("websocket: data hexdump", buffer, (total_length > WS_DATA_DUMP_SIZE) ? WS_DATA_DUMP_SIZE : total_length);
  if(total_length > WS_DATA_DUMP_SIZE) printf("  ...\n");
#endif

  return total_length;
}
