#include <websocket/websocket.h>

#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include <string.h>

#include <utils/utils.h>
#include <utils/sha1.h>
#include <utils/base64.h>

ws_server_t *ws_server_setup(unsigned int port, unsigned int queue)
{
  /* setup local TCP server */
  int fd = setup_local_server(port, queue);
  if(fd == -1)
  {
    return NULL;
  }

  ws_server_t *srv = (ws_server_t *) malloc(sizeof(ws_server_t));
  srv->srvfd = fd;
  srv->client.sock.sockfd = -1;

  return srv;
}

void ws_wait(ws_server_t *srv, ws_conn_handler_t handler)
{
  char buffer[1024];

  if(server_wait(srv->srvfd, &(srv->client.sock), 1) == -1)
  {
#ifndef WS_NO_ERROR
    printf("[ERROR] WebSocket server\n");
#endif
  }

#ifdef WS_VERBOSE
  printf("websocket: Client just connected, ip=%s\n", inet_ntoa(srv->client.sock.addr.sin_addr));
#endif

  recv(srv->client.sock.sockfd, buffer, 1024, 0);
  if(ws_handshake(srv, buffer, 1024) == -1)
  {
#ifndef WS_NO_ERROR
    printf("websocket: [ERROR] handshake error (connection with client is closed)\n");
#endif
    return;
  }
#ifdef WS_VERBOSE
  printf("websocket: Connection established successfully.\n");
#endif

  (*handler)(srv, &(srv->client));
}

int ws_handshake(ws_server_t *srv, char *buffer, size_t len)
{
#ifdef WS_VERBOSE_DETAILS
  printf("websocket: Analysing handshake...\n");
#endif

  /* analysing handshake */
  char *handshake = strndup(buffer, len);
  char *line = strsep(&handshake, "\n");
  if(line == NULL)
  {
#ifndef WS_NO_ERROR
    printf("websocket: [ERROR] failed to analyse buffer string\n");
#endif
    ws_close_client_early(&(srv->client), WS_CLOSE_BADREQUEST);

    return -1;
  }
  line[strlen(line)-1] = '\0';
  if(!startswith(line, "GET /test HTTP/1.1")) /* TODO: custom url */
  {
    /* error: wrong handshake protocol (must be a HTTP request) */
#ifndef WS_NO_ERROR
    printf("websocket: [ERROR] wrong handshake (\"%s\")", line);
#endif
    ws_close_client_early(&(srv->client), WS_CLOSE_BADREQUEST);

    return -1;
  }

  char *ws_key = NULL;
  int version = 0;
  int upgrade = 0;
  int ws;

  while((line = strsep(&handshake,"\n")) != NULL)
  {
    if(strlen(line) > 1024)
    {
      /* line size error */
#ifndef WS_NO_ERROR
      printf("websocket: [ERROR] line size error (%lu bytes)", strlen(line));
#endif
      ws_close_client_early(&(srv->client), WS_CLOSE_BADREQUEST);

      return -1;
    }
    line[strlen(line)-1] = '\0';

    if(startswith(line, "Connection:"))
    {
      upgrade = 1;

      if(strstr(line, "Upgrade") == NULL)
      {
        /* error: connection not upgraded */
#ifndef WS_NO_ERROR
        printf("websocket: [ERROR] connection not upgraded (%s)\n", line);
#endif
        ws_close_client_early(&(srv->client), WS_CLOSE_BADREQUEST);

        return -1;
      }
    } else if(startswith(line, "Upgrade:"))
    {
      ws = 1;

      if(strstr(line, "websocket") == NULL)
      {
        /* error: connection upgraded to wrong protocol */
#ifndef WS_NO_ERROR
        printf("websocket: [ERROR] connection upgraded to wrong protocol (%s)\n", line);
#endif
        ws_close_client_early(&(srv->client), WS_CLOSE_BADREQUEST);

        return -1;
      }
    } else if(startswith(line, "Host:"))
    {
#ifdef WS_VERBOSE_DETAILS
      printf("websocket: host for handshake is \"%s\"\n", line+6);
#endif
    } else if(startswith(line, "Sec-WebSocket-Key:"))
    {
      ws_key = strdup(strsplitlast(line, ':'));
    } else if(startswith(line, "Sec-WebSocket-Version:"))
    {
      version = atoi(strsplitlast(line, ':'));
    }
  }

  if(!upgrade || !ws)
  {
    /* error: wrong request */
#ifndef WS_NO_ERROR
    printf("websocket: [ERROR] wrong handshake request (upgrade=%d, ws=%d)\n", upgrade, ws);
#endif
    ws_close_client_early(&(srv->client), WS_CLOSE_BADREQUEST);

    return -1;
  }

#ifdef WS_VERBOSE_DETAILS
  printf("websocket: WebSocket version is %d\n", version);
  printf("websocket: WebSocket key is \"%s\"\n", ws_key);
#endif

  if(version != WS_VERSION)
  {
#ifndef WS_NO_ERROR
    printf("websocket: [ERROR] wrong WebSocket version (we expect version %d)\n", WS_VERSION);
#endif
    ws_close_client_early(&(srv->client), WS_CLOSE_WRONG_VERSION);

    return -1;
  }

  free(handshake);

#ifdef WS_VERBOSE
  printf("websocket: Validating handshake...\n");
#endif

  /* calculate accept key */
  /* acceptKey = base64(sha1(key + "258EAFA5-E914-47DA-95CA-C5AB0DC85B11"))
    where 258EAFA5-E914-47DA-95CA-C5AB0DC85B11 is the WebSocket magic number */

  SHA1_CTX sha;
  uint8_t sha1_res[20];
  char buf[256];
  int n;

  /* concatenating key with WebSocket magic number */
  strncpy(buf, ws_key, 91);
  strncat(buf, WS_MAGIC, 37);
  n = strlen(buf);

  /* calculating sha1 hash */
  SHA1Init(&sha);
  SHA1Update(&sha, (uint8_t *)buf, n);
  SHA1Final(sha1_res, &sha);

  /* encoding result in base64 */
  unsigned char *b64_res = base64_encode((unsigned char *) sha1_res, 20, NULL);
  if(b64_res[strlen((char *) b64_res)-1] == '\n') b64_res[strlen((char *) b64_res)-1] = '\0';
#ifdef WS_VERBOSE_DETAILS
  printf("websocket: Accept Key is \"%s\"\n", b64_res);
#endif

  /* send back accept message */
  snprintf(buf, 256, "HTTP/1.1 101 Switching Protocols\r\nUpgrade: websocket\r\nConnection: Upgrade\r\nSec-WebSocket-Accept: %s\r\n\r\n", b64_res);
  send(srv->client.sock.sockfd, buf, strlen(buf), 0);

  /* base64_encode returns a string which needs to be freed */
  free(b64_res);
  /* same for strdup */
  free(ws_key);

  /* connection is now established */

  return 0;
}

/*
 * Close a client when the WebSocket protocol is not
 * yet established.
 */
void ws_close_client_early(ws_client_t *client, int reason)
{
  char buf[512];

  if(reason == WS_CLOSE_BADREQUEST)
  {
    strncpy(buf, "HTTP/1.1 400 Bad Request\r\nConnection: close\r\n\r\n", 53);
    send(client->sock.sockfd, buf, strlen(buf), 0);
  } else if(reason == WS_CLOSE_WRONG_VERSION)
  {
    snprintf(buf, 85, "HTTP/1.1 400 Bad Request\r\nConnection: close\r\nSec-WebSocket-Version: %d\r\n\r\n", WS_VERSION);
    send(client->sock.sockfd, buf, strlen(buf), 0);
  }
#ifdef WS_VERBOSE
  printf("websocket: Closing client early (reason=%d)...\n", reason);
#endif
  close_client(&(client->sock));
  client->sock.sockfd = -1;
}

/*
 * Close a client whith the WebSocket protocol
 */
void ws_close_client(ws_client_t *client, int reason)
{
  char frame[512];
  size_t len = 2;

  struct ws_frame_header *frame_header = (struct ws_frame_header *) frame;
  frame_header->fin = 1;
  frame_header->rsv1 = 0;
  frame_header->rsv2 = 0;
  frame_header->rsv3 = 0;

  frame_header->opcode = WS_OPCODE_CONN_CLOSE;

  if(reason == WS_CLOSE_NORMAL)
  {
    /* 0x10 0x00 is normal closure code */
    ((uint8_t *) (((uintptr_t) frame) + sizeof(struct ws_frame_header)))[0] = 0x10;
    ((uint8_t *) (((uintptr_t) frame) + sizeof(struct ws_frame_header)))[1] = 0x00;
    len = 2;
  }

  frame_header->mask_and_len1 = len & WS_LEN1_MASK;
  /* send close frame */
  send(client->sock.sockfd, frame, sizeof(struct ws_frame_header) + (len & WS_LEN1_MASK), 0);

#ifdef WS_VERBOSE
  printf("websocket: Closing client (reason=%d)...\n", reason);
#endif
  close_client(&(client->sock));
  client->sock.sockfd = -1;
}

void ws_close_server(ws_server_t *srv)
{
  if(srv == NULL)
  {
    return;
  }

  /* TODO: close clients */
  if(srv->client.sock.sockfd > 0)
  {
    ws_close_client(&(srv->client), WS_CLOSE_NORMAL);
  }

#ifdef WS_VERBOSE
  printf("websocket: Closing server...\n");
#endif
  if(close(srv->srvfd) == -1)
  {
#ifndef WS_NO_ERROR
    printf("[ERROR] Failed to close server socket, errno=%d - \"%s\"\n", errno, strerror(errno));
#endif
  }

  free(srv);
}
