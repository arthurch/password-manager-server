#include <websocket/local_server.h>

#include <stdio.h>
#include <errno.h>
#include <string.h>

socketfd setup_local_server(unsigned int port, unsigned int queue)
{
  socketfd sock;
  struct sockaddr_in addr;

  sock = socket(AF_INET, SOCK_STREAM, 0);
  if(sock == -1)
  {
    printf("[ERROR] Failed to open server socket, errno=%d - \"%s\"\n", errno, strerror(errno));
    return -1;
  }

  addr.sin_family = AF_INET;
  addr.sin_port = htons(port);
  addr.sin_addr.s_addr = htonl(INADDR_LOOPBACK);

  if(bind(sock, (struct sockaddr *) &addr, sizeof(struct sockaddr_in)) == -1)
  {
    printf("[ERROR] Failed to bind server socket, errno=%d - \"%s\"\n", errno, strerror(errno));
    return -1;
  }

  if(listen(sock, queue) == -1)
  {
    printf("[ERROR] Can't listen on server socket, errno=%d - \"%s\"\n", errno, strerror(errno));
    return -1;
  }

  return sock;
}

int server_wait(socketfd srv, in_socket_t *client, int expect_local)
{
  client->addr_len = sizeof(struct sockaddr_in);
  client->sockfd = accept(srv, (struct sockaddr *) &(client->addr), &(client->addr_len));
  if(client->sockfd == -1)
  {
    printf("[ERROR] Failed to accept client, errno=%d - \"%s\"\n", errno, strerror(errno));
    return -1;
  }
  if(expect_local)
  {
    if(client->addr.sin_addr.s_addr != htonl(INADDR_LOOPBACK))
    {
      printf("[WARNING] Connection refused (client is not using loopback interface)\n");
      return -1;
    }
  }

  return client->sockfd;
}

void close_client(in_socket_t *client)
{
  if(close(client->sockfd) == -1)
  {
    printf("[ERROR] Failed to close client socket, errno=%d - \"%s\"\n", errno, strerror(errno));
  }
  client->sockfd = -1;
  memset(&client->addr, 0, sizeof(struct sockaddr_in));
}
