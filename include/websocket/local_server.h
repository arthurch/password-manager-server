#ifndef SERVER_H
#define SERVER_H

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/ip.h>
#include <arpa/inet.h>
#include <unistd.h>

typedef int socketfd;

struct in_socket {
  socketfd sockfd;
  struct sockaddr_in addr;
  socklen_t addr_len;
};
typedef struct in_socket in_socket_t;

socketfd setup_local_server(unsigned int port, unsigned int queue);
int server_wait(socketfd srv, in_socket_t *client, int expect_local);

void close_client(in_socket_t *client);

#endif
