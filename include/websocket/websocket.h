#ifndef WEBSOCKET_H
#define WEBSOCKET_H

#include <websocket/local_server.h>

/* choose verbose options */
#define WS_VERBOSE
// #define WS_VERBOSE_DETAILS
// #define WS_DATA_DUMP
#define WS_NO_WARNING
// #define WS_NO_ERROR

/* max size of data dumps */
#define WS_DATA_DUMP_SIZE 256

#define WS_VERSION 13
#define WS_MAGIC "258EAFA5-E914-47DA-95CA-C5AB0DC85B11"

/* length of buffer used to receive frames */
#define WS_BUFFER_SIZE 16384

/* connection close reasons */
#define WS_CLOSE_NORMAL 0
#define WS_CLOSE_BADREQUEST 1
#define WS_CLOSE_WRONG_VERSION 2

/* send flags */
#define WS_SEND_TYPE_MASK 0x1
#define WS_SEND_BINARY 0x0
#define WS_SEND_TEXT 0x1

#define WS_MASKINGKEY_SIZE 4

/* WebSocket frame header */
struct ws_frame_header {
  uint8_t opcode:4; /* interpretation of the current frame */

  uint8_t rsv3:1;   /* extension bit 3 */
  uint8_t rsv2:1;   /* extension bit 2 */
  uint8_t rsv1:1;   /* extension bit 1 */

  uint8_t fin:1;    /* fin bit indicates whether this is the last frame or not */

#define WS_OPCODE_CONTINUATION 0x0
#define WS_OPCODE_TEXT 0x1
#define WS_OPCODE_BINARY 0x2
/* 0x3-0x7 not used */
#define WS_OPCODE_CONN_CLOSE 0x8
#define WS_OPCODE_PING 0x9
#define WS_OPCODE_PONG 0xA
/* 0xB-0xF not used */

  uint8_t mask_and_len1:8; /* first bit is mask bit : set if payload is masked (should be 1 in client to server
    frame and 0 in server to client frame), remaining 7 bits are the first payload length indicator : if <126,
    this is the payload length, if == 126, the following two bytes are the payload length, if == 127, the
    following eight bytes are the payload length */
#define WS_MASK_MASK 0x80
#define WS_LEN1_MASK 0x7f

} __attribute__((packed));

struct ws_client {
  in_socket_t sock;
};
typedef struct ws_client ws_client_t;

struct ws_server {
  int srvfd;
  ws_client_t client; /* only one client for the moment */
};
typedef struct ws_server ws_server_t;

typedef void(*ws_conn_handler_t)(ws_server_t *srv, ws_client_t *client);

ws_server_t *ws_server_setup(unsigned int port, unsigned int queue);
void ws_close_server(ws_server_t *srv);

void ws_wait(ws_server_t *srv, ws_conn_handler_t handler);
int ws_handshake(ws_server_t *srv, char *handshake, size_t len);

int ws_recv(ws_client_t *client, void *buffer, size_t len, uint8_t *opcode);
int ws_send(ws_client_t *client, void *buffer, size_t len, int flag);
void ws_close_client_early(ws_client_t *client, int reason);
void ws_close_client(ws_client_t *client, int reason);

#endif
