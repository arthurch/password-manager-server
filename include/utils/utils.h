#ifndef UTILS_H
#define UTILS_H

#include <stddef.h>
#include <stdint.h>

uint64_t ntoh64(const uint64_t input);
uint64_t hton64(const uint64_t input);

int startswith(char *str, char *start);
char *strsplitlast(char *str, char split);

void hexdump(const char * desc, const void * addr, const size_t len);

#endif
