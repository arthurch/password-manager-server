CONFIG_CLEAN_EVERYTIME := true

CC := gcc
OUT := passman

INCLUDE_DIR := include/
BUILD_DIR := build/

SRC = $(wildcard src/*.c) \
	$(wildcard src/json/*.c) \
	$(wildcard src/websocket/*.c) \
	$(wildcard src/utils/*.c)
OBJS = $(SRC:.c=.o)

CFLAGS := -Wall -g -I$(INCLUDE_DIR)
LFLAGS :=

.PHONY: all clean cleanall

all: $(OUT)
	@echo "Done"
ifeq (true, $(CONFIG_CLEAN_EVERYTIME))
	@make clean
endif

### FILES TO BE BUILD ###
$(OUT): $(OBJS)
	@mkdir -p $(BUILD_DIR)
	@$(CC) -o $(BUILD_DIR)/$@ $(LFLAGS) $(OBJS)
	@echo "Linking complete"

%.o: %.c Makefile
	@$(CC) -MD $(CFLAGS) -c $< -o $@
	@echo "Compiled "$<" successfully"

### CLEANING ###
cleanall: clean
	@rm -rf $(BUILD_DIR)
	@echo "Removed build folder"

clean:
	@rm -f $(OBJS)
	@echo "Removed .o files"
	@rm -f $(OBJS:.o=.d)
	@echo "Removed .d files"

-include $(OBJS:.o=.d)
